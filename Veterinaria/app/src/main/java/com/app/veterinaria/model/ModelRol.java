package com.app.veterinaria.model;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Cananguchal Code on 01/08/2015.
 */
public class ModelRol {

    public HashMap<String,String> consultar(Spinner spiner,Context context){

        HashMap<String,String> spinnerMap = new HashMap<String, String>();
        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String json =serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetRols");

        try {
            JSONArray jsonArray = new JSONArray(json);
            String data[] = new String[jsonArray.length()];
            for(int i=0;i<jsonArray.length();i++){
                JSONObject rol = jsonArray.getJSONObject(i);
                data[i] = rol.getString("nombre");
                spinnerMap.put(rol.getString("nombre"),rol.getString("id"));
            }

            ArrayAdapter<String> adapter =new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item, data);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spiner.setAdapter(adapter);


        }catch (Exception er){}
        return  spinnerMap;
    }

    public int getPosicion(String id){

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String json =serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetRols");
        int posicion = 0;
        try {
            JSONArray jsonArray = new JSONArray(json);
            for(int i=0;i<jsonArray.length();i++){
                JSONObject rol = jsonArray.getJSONObject(i);
                if(rol.getString("id").equals(id)){
                    return  i;
                }
            }

        }catch (Exception er){}
        return  posicion;
    }

    public JSONObject buscar(String idBuscar){

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String data = serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetRol/"+idBuscar);
        try {
            return new JSONObject(data);
        } catch (JSONException e) {}
        return  null;
    }


}
