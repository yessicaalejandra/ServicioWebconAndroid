package com.app.veterinaria.model;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.app.veterinaria.tools.DataViewList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ModelPersonal {

    public String crear(String id,
                        String nombre,
                        String direccion,
                        String telefono,
                        String correo,
                        String numdoc,
                        String tipodoc,
                        String estado){

        String idNuevo = "-1";

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id",id );
            jsonObject.put("nombre",nombre);
            jsonObject.put("direccion", direccion);
            jsonObject.put("telefono", telefono);
            jsonObject.put("correo",correo);
            jsonObject.put("numdoc", numdoc);
            jsonObject.put("tipodoc",tipodoc);
            jsonObject.put("estado", estado);
            jsonObject.put("historials", new JSONArray());
            jsonObject.put("usuarios", new JSONArray());

            if(!Boolean.parseBoolean(serviceVeterinaria.PostrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/PersonalExiste", jsonObject.toString())))
            {
                idNuevo = serviceVeterinaria.PostrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/CrearPersonal", jsonObject.toString());
            }
        }catch (Exception er){}
        return idNuevo;
    }

    public boolean modificar(String id,
                             String nombre,
                             String direccion,
                             String telefono,
                             String correo,
                             String numdoc,
                             String tipodoc,
                             String estado){

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();

        try{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id",id );
            jsonObject.put("nombre",nombre);
            jsonObject.put("direccion", direccion);
            jsonObject.put("telefono", telefono);
            jsonObject.put("correo",correo);
            jsonObject.put("numdoc", numdoc);
            jsonObject.put("tipodoc",tipodoc);
            jsonObject.put("estado", estado);
            jsonObject.put("historials", new JSONArray());
            jsonObject.put("usuarios", new JSONArray());

            return Boolean.parseBoolean(serviceVeterinaria.PostrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/ModificarPersonal", jsonObject.toString()));

        }catch (Exception er){}
        return true;
    }

    public JSONObject buscar(String idBuscar){

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String data = serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetPersonal/"+idBuscar);
        try {
            return new JSONObject(data);
        } catch (JSONException e) {}
        return  null;
    }

    public ArrayList<DataViewList> consultar(){
        ArrayList<DataViewList> list = new ArrayList<DataViewList>();

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String json =serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetPersonals");

        try {
            JSONArray jsonArray = new JSONArray(json);
            for(int i=0;i<jsonArray.length();i++){
                JSONObject mascota = jsonArray.getJSONObject(i);
                list.add(new DataViewList(mascota.getString("nombre"),mascota.getString("numdoc"),mascota.getString("id")));
            }
        }catch (Exception er){}
        return list;
    }

    public HashMap<String,String> consultarMap(){
        HashMap<String,String> spinnerMap = new HashMap<String, String>();
        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String json =serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetPersonals");

        try {
            JSONArray jsonArray = new JSONArray(json);
            for(int i=0;i<jsonArray.length();i++){
                JSONObject mascota = jsonArray.getJSONObject(i);
                spinnerMap.put(mascota.getString("id"),mascota.getString("numdoc"));
            }

        }catch (Exception er){}
        return  spinnerMap;
    }

    public HashMap<String,String> consultar(Spinner spiner,Context context){

        HashMap<String,String> spinnerMap = new HashMap<String, String>();
        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String json =serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetPersonals");

        try {
            JSONArray jsonArray = new JSONArray(json);
            String data[] = new String[jsonArray.length()];
            for(int i=0;i<jsonArray.length();i++){
                JSONObject personal = jsonArray.getJSONObject(i);
                data[i] = personal.getString("nombre");
                spinnerMap.put(personal.getString("nombre"),personal.getString("id"));
            }

            ArrayAdapter<String> adapter =new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item, data);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spiner.setAdapter(adapter);


        }catch (Exception er){}
        return  spinnerMap;
    }

    public int getPosicion(String id){

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String json =serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetPersonals");
        int posicion = 0;
        try {
            JSONArray jsonArray = new JSONArray(json);
            for(int i=0;i<jsonArray.length();i++){
                JSONObject personal = jsonArray.getJSONObject(i);
                if(personal.getString("id").equals(id)){
                    return  i;
                }
            }

        }catch (Exception er){}
        return  posicion;
    }

}
