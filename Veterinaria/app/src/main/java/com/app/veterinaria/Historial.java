package com.app.veterinaria;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TimePicker;
import android.widget.Toast;

import com.app.veterinaria.model.ModelHistorial;
import com.app.veterinaria.model.ModelMascota;
import com.app.veterinaria.model.ModelPersonal;
import com.app.veterinaria.model.ModelPropietario;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
import java.util.HashMap;


public class Historial extends ActionBarActivity {

    private HashMap<String,String>  mascotas,personals;

    private String id,idPersonal;//,docPersonal;
    private ModelHistorial modelHistorial;
    private ModelMascota modelMascota;
    private ModelPersonal modelPersonal;
    private Spinner fkMascota;
    private EditText fkPersonal, peso,diagnostico,anamesia,tratamiento,vacuna;
    private DatePicker fecha,fechaVacuna;
    private TimePicker hora;


    private Button btnOpe;
    private int operacion;
    private String idPara;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial);

        modelHistorial = new ModelHistorial();
        modelMascota = new ModelMascota();
        modelPersonal = new ModelPersonal();

        Bundle bundle = getIntent().getExtras();
        idPara = bundle.getString("id") == null ? "-1": bundle.getString("id");
        operacion = bundle.getInt("operacion");
        idPersonal =  bundle.getString("idPersonal");
        //docPersonal =  bundle.getString("documento");

        loadComponent();
        loadMascota();
        loadPersonal();
        llamarEstado();
    }

    public  void llamarEstado(){
        switch (operacion){
            case 1:
                loadValueComponent(idPara);
                estadoVer();
                break;
            case 2:
                estadoCrear();
                break;
            case 3:
                loadValueComponent(idPara);
                estadoModificar();
                break;
        }
    }

    public void clickBtn(View view){
        switch (operacion){
            case 2:
                crear();
                break;

            case 3:
                modificar();
                break;
        }
    }

    private void estadoModificar() {
        fkPersonal.setEnabled(false);
        fkMascota.setEnabled(false);
        btnOpe.setText("Modificar");
    }

    private void estadoCrear() {

        fkPersonal.setText(personals.get(idPersonal));
        fkPersonal.setEnabled(false);
    }

    private void estadoVer() {

        fkPersonal.setEnabled(false);
        fkMascota.setEnabled(false);
        peso.setEnabled(false);
        diagnostico.setEnabled(false);
        anamesia.setEnabled(false);
        tratamiento.setEnabled(false);
        fecha.setEnabled(false);
        fechaVacuna.setEnabled(false);
        hora.setEnabled(false);
        btnOpe.setVisibility(View.INVISIBLE);
        vacuna.setEnabled(false);
    }

    public void loadComponent(){

        fkPersonal = (EditText) findViewById(R.id.editText11);
        fkMascota = (Spinner) findViewById(R.id.spinner3);
        peso = (EditText) findViewById(R.id.editText7);
        diagnostico = (EditText) findViewById(R.id.editText8);
        anamesia = (EditText) findViewById(R.id.editText9);
        tratamiento  = (EditText) findViewById(R.id.editText10);
        fecha = (DatePicker) findViewById(R.id.datePicker);
        fechaVacuna = (DatePicker) findViewById(R.id.datePicker2);
        hora = (TimePicker) findViewById(R.id.timePicker);
        vacuna = (EditText) findViewById(R.id.editText13);
        btnOpe = (Button) findViewById(R.id.button10);
    }

    private void loadValueComponent(String idPara) {

        JSONObject jsonObject = modelHistorial.buscar(idPara);

        try{
            id = idPara;
            idPersonal = jsonObject.getString("fkpersonal");

            fkPersonal.setText(personals.get(jsonObject.getString("fkpersonal")));
            int posicion =  modelMascota.getPosicion(jsonObject.getString("fkmascota"));
            fkMascota.setSelection(posicion);

            peso.setText(jsonObject.getString("peso"));
            diagnostico.setText(jsonObject.getString("diagnostico"));
            anamesia.setText(jsonObject.getString("anamesia"));
            tratamiento.setText(jsonObject.getString("tratamiento"));
            vacuna.setText(jsonObject.getString("vacuna"));

            DateFormat df  = new SimpleDateFormat("yyyy-MM-dd");

            try {

                Date fechaDate = df.parse(jsonObject.getString("fecha"));
                Calendar  calendarDate = Calendar.getInstance();
                calendarDate.setTime(fechaDate);
                fecha.updateDate(calendarDate.get(Calendar.YEAR), calendarDate.get(Calendar.MONTH), calendarDate.get(Calendar.DATE));

                Date fechaDateVacuna = df.parse(jsonObject.getString("fechavacuna"));
                Calendar  calendarVacuna = Calendar.getInstance();
                calendarVacuna.setTime(fechaDateVacuna);
                fechaVacuna.updateDate(calendarVacuna.get(Calendar.YEAR), calendarVacuna.get(Calendar.MONTH), calendarVacuna.get(Calendar.DATE));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            int horaInt = Integer.parseInt(jsonObject.getString("hora").split(";")[0]);
            int minutosInt = Integer.parseInt(jsonObject.getString("hora").split(";")[1]);
            hora.setCurrentHour(horaInt);
            hora.setCurrentMinute(minutosInt);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public  void loadMascota(){
        mascotas = modelMascota.consultar(fkMascota,this);
    }

    public  void loadPersonal(){
        personals = modelPersonal.consultarMap();
    }

    public void crear(){


        Calendar calendarDate = Calendar.getInstance();
        calendarDate.set(fecha.getYear(), fecha.getMonth(), fecha.getDayOfMonth());

        Calendar calendarVacuna = Calendar.getInstance();
        calendarVacuna.set(fechaVacuna.getYear(),fechaVacuna.getMonth(),fechaVacuna.getDayOfMonth());

        String fechaDate =  "/Date("+calendarDate.getTime().getTime()+")/";
        String fechaVacuna = "/Date("+calendarVacuna.getTime().getTime()+")/";

        String horaFormato = hora.getCurrentHour() + ";" + hora.getCurrentMinute();

        String idNew  = modelHistorial.crear(
                id,
                mascotas.get(fkMascota.getSelectedItem().toString()),
                idPersonal,
                tratamiento.getText().toString(),
                fechaDate,
                horaFormato,
                peso.getText().toString(),
                anamesia.getText().toString(),
                vacuna.getText().toString(),
                fechaVacuna,
                diagnostico.getText().toString()
        );

        if(!idNew.equals("-1")){
            Toast.makeText(getApplicationContext(), "Guardado correctamente", Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(getApplicationContext(), "Error al guardar", Toast.LENGTH_LONG).show();
        }

    }

    public  void modificar(){

        Calendar calendarDate = Calendar.getInstance();
        calendarDate.set(fecha.getYear(), fecha.getMonth(), fecha.getDayOfMonth());

        Calendar calendarVacuna = Calendar.getInstance();
        calendarVacuna.set(fechaVacuna.getYear(),fechaVacuna.getMonth(),fechaVacuna.getDayOfMonth());

        String fechaDate =  "/Date("+calendarDate.getTime().getTime()+")/";
        String fechaVacuna = "/Date("+calendarVacuna.getTime().getTime()+")/";

        String horaFormato = hora.getCurrentHour() + ";" + hora.getCurrentMinute();

        Log.e("mascotas",mascotas.toString());
        Log.e("fkMascota",fkMascota.getSelectedItem().toString());

        boolean verificacion  = modelHistorial.modificar(
                id,
                mascotas.get(fkMascota.getSelectedItem().toString()),
                idPersonal,
                tratamiento.getText().toString(),
                fechaDate,
                horaFormato,
                peso.getText().toString(),
                anamesia.getText().toString(),
                vacuna.getText().toString(),
                fechaVacuna,
                diagnostico.getText().toString()
        );

        if(verificacion){
            Toast.makeText(getApplicationContext(), "Modificado correctamente", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(getApplicationContext(), "Error al modificar", Toast.LENGTH_LONG).show();
        }

    }


}
