package com.app.veterinaria;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.app.veterinaria.model.ModelMascota;
import com.app.veterinaria.model.ModelMascotaPropietario;
import com.app.veterinaria.model.ModelPropietario;
import com.app.veterinaria.model.ServiceVeterinaria;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class Mascota extends ActionBarActivity {

    private EditText id,nombre,raza,especie,color,edad;
    private Switch estado;
    private Spinner cbSexo,cbPropietario;
    private Button btnOpe;
    private int operacion;
    private ModelMascota modelMascota;
    private ModelPropietario modelPropietario;
    private ModelMascotaPropietario modelMascotaPropietario;
    private HashMap<String,String> propietarios;
    private String idPara;




    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mascota);

        modelMascota = new ModelMascota();
        modelPropietario = new ModelPropietario();
        modelMascotaPropietario = new ModelMascotaPropietario();

        Bundle bundle = getIntent().getExtras();
        idPara = bundle.getString("id") == null ? "-1": bundle.getString("id");
        operacion = bundle.getInt("operacion");

        loadComponent();
        loadComboBoxSexo();
        loadPropietario();

        llamarEstado();
    }

    public  void llamarEstado(){
        switch (operacion){
            case 1:
                loadValueComponent(idPara);
                estadoVer();
                break;
            case 2:
                estadoCrear();
                break;
            case 3:
                loadValueComponent(idPara);
                estadoModificar();
                break;
        }
    }

    public void clickBtn(View view){
        switch (operacion){
            case 2:
                crear();
                break;

            case 3:
                modificar();
                break;
        }
    }

    public void loadComponent(){
        id = (EditText) findViewById(R.id.textId);
        nombre = (EditText) findViewById(R.id.textNombre);
        raza = (EditText) findViewById(R.id.textRaza);
        especie = (EditText) findViewById(R.id.textEspecie);
        edad = (EditText) findViewById(R.id.textEdad);
        color = (EditText) findViewById(R.id.textColor);
        cbSexo = (Spinner) findViewById(R.id.cbSexo);
        cbPropietario = (Spinner) findViewById(R.id.cbPropietario);
        estado = (Switch) findViewById(R.id.switch1);
        btnOpe  = (Button) findViewById(R.id.btnSaveMascota);
    }

    public void loadComboBoxSexo(){
        List<String> listSexo = new ArrayList<String>();
        listSexo.add("Macho");
        listSexo.add("Hembra");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listSexo);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cbSexo.setAdapter(dataAdapter);
    }

    public void estadoVer(){
        id.setEnabled(false);
        nombre.setEnabled(false);
        raza.setEnabled(false);
        especie.setEnabled(false);
        edad.setEnabled(false);
        color.setEnabled(false);
        cbSexo.setEnabled(false);
        cbPropietario.setEnabled(false);
        estado.setEnabled(false);
        btnOpe.setVisibility(View.INVISIBLE);
    }


    public  void estadoModificar(){
        id.setEnabled(true);
        btnOpe.setText("Modificar");
    }

    public  void estadoCrear(){
        id.setText("0");
        id.setVisibility(View.INVISIBLE);
        btnOpe.setText("Guardar");
    }

    public  void loadValueComponent(String idPara){

        JSONObject jsonObject = modelMascota.buscar(idPara);

        try {

            id.setText(jsonObject.getString("id"));
            nombre.setText(jsonObject.getString("nombre"));
            raza.setText(jsonObject.getString("raza"));
            especie.setText(jsonObject.getString("especie"));
            edad.setText(jsonObject.getString("edad"));
            color.setText(jsonObject.getString("color"));
            cbSexo.setSelection(jsonObject.getInt("sexo"));
            estado.setChecked(jsonObject.getString("estado").equals("1") ? true : false);

            JSONObject jsonPropietario =  modelMascotaPropietario.buscar(id.getText().toString());
            int posicion =  modelPropietario.getPosicion(jsonPropietario.getString("fkpropietario"));
            cbPropietario.setSelection(posicion);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void loadPropietario(){
        propietarios =  modelPropietario.consultar(cbPropietario,this);
    }

    public void crear(){

        if(validarCampos()) {
            int intEstado = estado.isChecked() ? 1 : 0;

            String idNew = modelMascota.crear(
                    id.getText().toString(),
                    nombre.getText().toString(),
                    especie.getText().toString(),
                    raza.getText().toString(),
                    edad.getText().toString(),
                    color.getText().toString(),
                    String.valueOf(cbSexo.getSelectedItemPosition()),
                    String.valueOf(intEstado)
            );
            modelMascotaPropietario.crear(idNew, propietarios.get(cbPropietario.getSelectedItem().toString()));

            id.setText(idNew);
            id.setVisibility(View.VISIBLE);
            id.setEnabled(false);

            if (!idNew.equals("-1")) {
                Toast.makeText(getApplicationContext(), "Guardado correctamente", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Error al guardar", Toast.LENGTH_LONG).show();
            }
        }
    }

    public  void modificar(){

        if(validarCampos()) {
            int intEstado = estado.isChecked() ? 1 : 0;

            boolean verificacionMascota = modelMascota.modificar(
                    id.getText().toString(),
                    nombre.getText().toString(),
                    especie.getText().toString(),
                    raza.getText().toString(),
                    edad.getText().toString(),
                    color.getText().toString(),
                    String.valueOf(cbSexo.getSelectedItemPosition()),
                    String.valueOf(intEstado)
            );

            boolean verificacionPropietario = modelMascotaPropietario.modificar(id.getText().toString(), propietarios.get(cbPropietario.getSelectedItem().toString()));

            if (verificacionMascota && verificacionPropietario) {
                Toast.makeText(getApplicationContext(), "Modificado correctamente", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Error al modificar", Toast.LENGTH_LONG).show();
            }
        }

    }

    public  boolean validarCampos(){

        if(nombre.getText().toString().trim().toLowerCase().equals("null") || nombre.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "El campo nombre es obligatorio", Toast.LENGTH_LONG).show();
            return false;
        }
        if(raza.getText().toString().trim().toLowerCase().equals("null") || raza.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "El campo raza es obligatorio", Toast.LENGTH_LONG).show();
            return false;
        }

        if(especie.getText().toString().trim().toLowerCase().equals("null") || especie.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "El campo especie es obligatorio", Toast.LENGTH_LONG).show();
            return false;
        }
        if(color.getText().toString().trim().toLowerCase().equals("null") || color.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "El campo color es obligatorio", Toast.LENGTH_LONG).show();
            return false;
        }
        if(edad.getText().toString().trim().toLowerCase().equals("null") || edad.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "El campo edad es obligatorio", Toast.LENGTH_LONG).show();
            return false;
        }

        return  true;
    }

}
