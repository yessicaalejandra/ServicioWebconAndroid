package com.app.veterinaria;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;

import com.app.veterinaria.model.ModelPersonal;
import com.app.veterinaria.tools.AdapterListBean;
import com.app.veterinaria.tools.DataViewList;

import java.util.ArrayList;
import java.util.Locale;


public class ListPersonal extends ActionBarActivity {

    private ModelPersonal modelPersonal;
    private ListView listData;
    private EditText textBuscar;
    private AdapterListBean adapterListBean;
    private int operacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_personal);

        Bundle bundle = getIntent().getExtras();
        operacion = bundle.getInt("operacion");

        modelPersonal = new ModelPersonal();

        listData = (ListView) findViewById(R.id.listView2);
        textBuscar = (EditText) findViewById(R.id.editText6);

        try {
            ArrayList<DataViewList> dataViewLists =  modelPersonal.consultar();
            if(dataViewLists != null) {
                adapterListBean = new AdapterListBean(this, dataViewLists, Personal.class, "Nombre: ", "ID: ", true, operacion);
                listData.setAdapter(adapterListBean);
            }

            textBuscar.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable arg0) {
                    String text = textBuscar.getText().toString().toLowerCase(Locale.getDefault());
                    adapterListBean.filter(text);
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1,
                                              int arg2, int arg3) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                    // TODO Auto-generated method stub
                }
            });
        }catch (Exception er){
            Log.e("errExceptio", er.getMessage());
        }

    }


}
