package com.app.veterinaria.model;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.app.veterinaria.tools.DataViewList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ModelMascota {



    public String crear(String id,
                      String nombre,
                      String especie,
                      String raza,
                      String edad,
                      String color,
                      String sexo,
                         String estado){

        String idNuevo = "-1";
        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", Integer.parseInt(id));
            jsonObject.put("nombre", nombre);
            jsonObject.put("especie", especie);
            jsonObject.put("raza", raza);
            jsonObject.put("edad", Integer.parseInt(edad));
            jsonObject.put("color", color);
            jsonObject.put("sexo", sexo);
            jsonObject.put("estado", estado);
            jsonObject.put("historials", new JSONArray());
            jsonObject.put("mascotapropietarios",new JSONArray());

            idNuevo =  serviceVeterinaria.PostrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/CrearMascota", jsonObject.toString());

        }catch (Exception er){}
        return  idNuevo;
    }

    public boolean modificar(String id,
                          String nombre,
                          String especie,
                          String raza,
                          String edad,
                          String color,
                          String sexo,
                             String estado){

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", Integer.parseInt(id));
            jsonObject.put("nombre", nombre);
            jsonObject.put("especie", especie);
            jsonObject.put("raza", raza);
            jsonObject.put("edad", Integer.parseInt(edad));
            jsonObject.put("color", color);
            jsonObject.put("sexo", sexo);
            jsonObject.put("estado", Integer.parseInt(estado));
            jsonObject.put("historials", new JSONArray());
            jsonObject.put("mascotapropietarios", new JSONArray());

            return Boolean.parseBoolean(serviceVeterinaria.PostrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/ModificarMascota", jsonObject.toString()));

        }catch (Exception er){}
        return false;
    }

    public JSONObject buscar(String idMascota){

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String data = serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetMascota/"+idMascota);
        try {
            return new JSONObject(data);
        } catch (JSONException e) {}
        return  null;
    }

    public ArrayList<DataViewList> consultar(){

        ArrayList<DataViewList> list = new ArrayList<DataViewList>();

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String json =serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetMascotas");

        try {
            JSONArray jsonArray = new JSONArray(json);
            for(int i=0;i<jsonArray.length();i++){
                JSONObject mascota = jsonArray.getJSONObject(i);
                list.add(new DataViewList(mascota.getString("nombre"),mascota.getString("id"),mascota.getString("id")));
            }
        }catch (Exception er){}
        return list;
    }

    public HashMap<String,String> consultar(Spinner spiner,Context context){

        HashMap<String,String> spinnerMap = new HashMap<String, String>();
        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String json =serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetMascotas");

        try {
            JSONArray jsonArray = new JSONArray(json);
            String data[] = new String[jsonArray.length()];
            for(int i=0;i<jsonArray.length();i++){
                JSONObject mascota = jsonArray.getJSONObject(i);
                data[i] = mascota.getString("nombre");
                spinnerMap.put(mascota.getString("nombre"),mascota.getString("id"));
            }

            ArrayAdapter<String> adapter =new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item, data);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spiner.setAdapter(adapter);

        }catch (Exception er){}
        return  spinnerMap;
    }

    public int getPosicion(String id){
        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String json =serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetMascotas");
        int posicion = 0;
        try {
            JSONArray jsonArray = new JSONArray(json);
            for(int i=0;i<jsonArray.length();i++){
                JSONObject mascota = jsonArray.getJSONObject(i);
                if(mascota.getString("id").equals(id)){
                    return  i;
                }
            }

        }catch (Exception er){}
        return  posicion;
    }

}
