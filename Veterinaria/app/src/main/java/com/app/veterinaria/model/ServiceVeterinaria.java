package com.app.veterinaria.model;

import android.support.annotation.RequiresPermission;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentProducer;
import org.apache.http.entity.EntityTemplate;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;


public class ServiceVeterinaria {

    public static String jsonRequest;

    public String GetrequestService(final String rutaRecurso)  {
        jsonRequest = "";
        Thread hilo =  new Thread(new Runnable(){
               @Override
               public void run() {
                   try {
                       HttpClient httpClient = new DefaultHttpClient();
                       HttpGet method =  new HttpGet( new URI(rutaRecurso) );
                       HttpResponse response = httpClient.execute(method);
                       if ( response != null )
                       {
                           jsonRequest = getResponse(response.getEntity());
                       }
                   } catch (Exception ex) {
                       ex.printStackTrace();
                   }
               }
        });
        hilo.start();
        while (hilo.isAlive()){}
        return jsonRequest;
    }

    public String PostrequestService(final String rutaRecurso, final String data) {
        jsonRequest = "";
        Thread hilo =  new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost method =  new HttpPost( new URI(rutaRecurso));
                    StringEntity stringEntity = new StringEntity(data,"UTF-8");
                    //stringEntity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    //stringEntity.setContentType( new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    method.setHeader ("content-type","application/json");
                    method.setEntity(stringEntity);
                    HttpResponse response = httpClient.execute(method);
                    if ( response != null )
                    {
                        jsonRequest = getResponse(response.getEntity());
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        hilo.start();
        while (hilo.isAlive()){}
        return jsonRequest;
    }

    private String getResponse( HttpEntity entity )
    {
        String response = "";

        try
        {
            int length = ( int ) entity.getContentLength();
            StringBuffer sb = new StringBuffer( length );
            InputStreamReader isr = new InputStreamReader( entity.getContent(), "UTF-8" );
            char buff[] = new char[length];
            int cnt;
            while ( ( cnt = isr.read( buff, 0, length - 1 ) ) > 0 )
            {
                sb.append( buff, 0, cnt );
            }
            response = sb.toString();
            isr.close();
        } catch ( IOException ioe ) {
            ioe.printStackTrace();
        }
        return response;
    }



}
