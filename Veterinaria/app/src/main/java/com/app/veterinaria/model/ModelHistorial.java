package com.app.veterinaria.model;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.app.veterinaria.tools.DataViewList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class ModelHistorial {

    public String crear(
            String id,
            String fkmascota,
            String fkpersonal,
            String tratamiento,
            String fecha,
            String hora,
            String peso,
            String anamesia,
            String vacuna,
            String fechaVacuna,
            String diagnostico){

        String idNuevo = "-1";

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id",id );
            jsonObject.put("fkmascota",fkmascota);
            jsonObject.put("fkpersonal", fkpersonal);
            jsonObject.put("tratamiento", tratamiento);
            jsonObject.put("fecha",fecha);
            jsonObject.put("hora", hora);
            jsonObject.put("peso",peso);
            jsonObject.put("vacuna",vacuna);
            jsonObject.put("anamesia", anamesia);
            jsonObject.put("fechavacuna",fechaVacuna);
            jsonObject.put("diagnostico",diagnostico);

            Log.i("DataHistorial", jsonObject.toString());

            idNuevo =  serviceVeterinaria.PostrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/CrearHistorial", jsonObject.toString());

        }catch (Exception er){}
        return idNuevo;
    }

    public boolean modificar(String id,
                             String fkmascota,
                             String fkpersonal,
                             String tratamiento,
                             String fecha,
                             String hora,
                             String peso,
                             String anamesia,
                             String vacuna,
                             String fechaVacuna,
                             String diagnostico){

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();

        try{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id",id );
            jsonObject.put("fkmascota",fkmascota);
            jsonObject.put("fkpersonal", fkpersonal);
            jsonObject.put("tratamiento", tratamiento);
            jsonObject.put("fecha",fecha);
            jsonObject.put("hora", hora);
            jsonObject.put("peso",peso);
            jsonObject.put("vacuna",vacuna);
            jsonObject.put("anamesia", anamesia);
            jsonObject.put("fechavacuna",fechaVacuna);
            jsonObject.put("diagnostico",diagnostico);

            Log.e("jsonModificar",jsonObject.toString());

            return Boolean.parseBoolean(serviceVeterinaria.PostrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/ModificarHistorial", jsonObject.toString()));

        }catch (Exception er){

            Log.e("ErrorHistorial",er.getMessage());
        }
        return true;
    }

    public JSONObject buscar(String idBuscar){

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String data = serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetHistorial/"+idBuscar);
        try {
            return new JSONObject(data);
        } catch (JSONException e) {}
        return  null;
    }

    public ArrayList<DataViewList> consultar(){
        ArrayList<DataViewList> list = new ArrayList<DataViewList>();

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String json =serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetHistorials");

        try {
            JSONArray jsonArray = new JSONArray(json);
            for(int i=0;i<jsonArray.length();i++){
                JSONObject historial = jsonArray.getJSONObject(i);
                list.add(new DataViewList(historial.getString("id"),historial.getString("fkmascota"),historial.getString("id")));
            }
        }catch (Exception er){}
        return list;
    }

    public HashMap<String,String> consultar(Spinner spiner,Context context){

        String data[] = new String[4];
        data[0] = "Carlos";
        data[1] = "Jose";
        data[2] = "Juanes";
        data[3] = "Yeison";

        String id[] = new String[4];
        id[0]="123";
        id[1]="124";
        id[2]="125";
        id[3]="126";

        HashMap<String,String> spinnerMap = new HashMap<String, String>();
        for(int i=0;i<data.length;i++){
            spinnerMap.put(data[i],id[i]);
        }

        ArrayAdapter<String> adapter =new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spiner.setAdapter(adapter);

        return  spinnerMap;
    }

}
