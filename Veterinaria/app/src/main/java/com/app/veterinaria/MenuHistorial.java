package com.app.veterinaria;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;


public class MenuHistorial extends ActionBarActivity {

    private  String idPersonal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_historial);

        Button button = (Button) findViewById(R.id.button17);//activar modificar
        button.setVisibility(View.INVISIBLE);

        Bundle bundle = getIntent().getExtras();
        idPersonal = bundle.getString("personalid");
    }

    public void irRegistrar(View view) {
        Intent intent  = new Intent(this, Historial.class);
        intent.putExtra("operacion",2);
        intent.putExtra("idPersonal", idPersonal);
        startActivity(intent);
    }

    public void irModificar(View view) {
        Intent intent  = new Intent(this, ListHistorial.class);
        intent.putExtra("operacion",3);
        startActivity(intent);
    }

    public void irVer(View view) {
        Intent intent  = new Intent(this, ListHistorial.class);
        intent.putExtra("operacion",1);
        startActivity(intent);
    }
}
