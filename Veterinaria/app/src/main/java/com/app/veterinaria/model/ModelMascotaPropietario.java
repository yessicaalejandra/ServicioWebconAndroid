package com.app.veterinaria.model;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;


public class ModelMascotaPropietario {

    public boolean crear(String idMascota,String idPropietario){

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("fkmascota", Integer.parseInt(idMascota.replace("\""," ").trim()));
            jsonObject.put("fkpropietario", Integer.parseInt(idPropietario));
            jsonObject.put("fecha", "/Date(" + new Date().getTime() + ")/");

            serviceVeterinaria.PostrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/CrearMascotaPropietario", jsonObject.toString());
            return  true;
        }catch (Exception er){
            Log.e("Error mp",er.getMessage());
            return  false;
        }
    }


    public JSONObject buscar(String id){
        // en el servicio que traiga al de la fecha mas actaul
        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String data = serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetMascotaPropietario/"+id);
        try {
            return new JSONObject(data);
        } catch (JSONException e) {}
        return  null;
    }

    public boolean modificar(String idMascota,String idPropietario){

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        JSONObject data =  buscar(idMascota);
        if(data != null){
            try{
                if(!data.getString("fkmascota").equals(idMascota) || !data.getString("fkpropietario").equals(idPropietario)){
                    crear(idMascota,idPropietario);
                    return  true;
                }else{
                    try{
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("fkmascota", Integer.parseInt(idMascota.replace("\""," ").trim()));
                        jsonObject.put("fkpropietario", Integer.parseInt(idPropietario));
                        jsonObject.put("fecha", "/Date(" + new Date().getTime() + ")/");

                        serviceVeterinaria.PostrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/ModificarMascotaPropietario", jsonObject.toString());
                        return  true;
                    }catch (Exception er){}
                    return  false;
                }
            }catch (Exception er){}
        }
        return false;
    }

}
