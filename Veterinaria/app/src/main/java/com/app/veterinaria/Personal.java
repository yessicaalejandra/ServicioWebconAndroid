package com.app.veterinaria;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.app.veterinaria.model.ModelPersonal;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Personal extends ActionBarActivity {

    private String id;
    private EditText nombre,direccion,telefono,correo,numdoc;
    private Spinner cbTipo;
    private Switch estado;
    private Button btnOpe;
    private int operacion;
    private ModelPersonal modelPersonal;
    private String idPara;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal);

        modelPersonal = new ModelPersonal();

        loadComponent();
        loadTipo();

        Bundle bundle = getIntent().getExtras();
        idPara = bundle.getString("id") == null ? "-1": bundle.getString("id");
        operacion = bundle.getInt("operacion");

        llamarEstado();

    }

    public  void llamarEstado(){

        switch (operacion){
            case 1:
                loadValueComponent(idPara);
                estadoVer();
                break;
            case 2:
                estadoCrear();
                break;
            case 3:
                loadValueComponent(idPara);
                estadoModificar();
                break;
        }
    }

    public void loadTipo(){

        List<String> listTipo = new ArrayList<String>();
        listTipo.add("Cedula");
        listTipo.add("Tarjeta Identidad");
        listTipo.add("Pasaporte");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listTipo);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cbTipo.setAdapter(dataAdapter);
    }

    private void estadoModificar() {
        btnOpe.setText("Modificar");
    }

    private void estadoCrear() {
        btnOpe.setText("Guardar");
    }

    private void estadoVer() {
        nombre.setEnabled(false);
        direccion.setEnabled(false);
        correo.setEnabled(false);
        telefono.setEnabled(false);
        numdoc.setEnabled(false);
        estado.setEnabled(false);
        cbTipo.setEnabled(false);
        btnOpe.setVisibility(View.INVISIBLE);
    }

    public void loadComponent(){

        nombre = (EditText) findViewById(R.id.editText2);
        direccion = (EditText) findViewById(R.id.editText3);
        correo = (EditText) findViewById(R.id.editText5);
        telefono = (EditText) findViewById(R.id.editText4);
        numdoc = (EditText) findViewById(R.id.editText);
        estado = (Switch) findViewById(R.id.switch3);
        cbTipo = (Spinner) findViewById(R.id.spinner);
        btnOpe = (Button) findViewById(R.id.button9);
    }

    public void clickBtn(View view){
        switch (operacion){
            case 2:
                crear();
                break;

            case 3:
                modificar();
                break;
        }
    }

    private void modificar() {

        if(validarCampos()) {
            int intEstado = estado.isChecked() ? 1 : 0;

            boolean verificacion = modelPersonal.modificar(
                    id,
                    nombre.getText().toString(),
                    direccion.getText().toString(),
                    telefono.getText().toString(),
                    correo.getText().toString(),
                    numdoc.getText().toString(),
                    String.valueOf(cbTipo.getSelectedItemPosition()),
                    String.valueOf(intEstado)
            );

            if (verificacion) {
                Toast.makeText(getApplicationContext(), "Modificado correctamente", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Error al modificar", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void crear() {

        if(validarCampos()) {

            int intEstado = estado.isChecked() ? 1 : 0;

            String codNew = modelPersonal.crear(
                    "0",
                    nombre.getText().toString(),
                    direccion.getText().toString(),
                    telefono.getText().toString(),
                    correo.getText().toString(),
                    numdoc.getText().toString(),
                    String.valueOf(cbTipo.getSelectedItemPosition()),
                    String.valueOf(intEstado)
            );

            if (!codNew.equals("-1")) {
                Toast.makeText(getApplicationContext(), "Guardado correctamente", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Error Personal ya existe o Datos no validos", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void loadValueComponent(String idPara) {

        JSONObject jsonObject = modelPersonal.buscar(idPara);
        if(jsonObject != null){
            try {
                id = idPara;
                numdoc.setText(jsonObject.getString("numdoc"));
                cbTipo.setSelection(jsonObject.getInt("tipodoc"));
                nombre.setText(jsonObject.getString("nombre"));
                direccion.setText(jsonObject.getString("direccion"));
                telefono.setText(jsonObject.getString("telefono"));
                correo.setText(jsonObject.getString("correo"));
                estado.setChecked(jsonObject.getString("estado").equals("1") ? true : false);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public  boolean validarCampos(){

        if(nombre.getText().toString().trim().toLowerCase().equals("null") || nombre.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "El campo nombre es obligatorio", Toast.LENGTH_LONG).show();
            return false;
        }

        if(numdoc.getText().toString().trim().toLowerCase().equals("null") || numdoc.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "El campo documento es obligatorio", Toast.LENGTH_LONG).show();
            return false;
        }

        if(direccion.getText().toString().trim().toLowerCase().equals("null") || direccion.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "El campo direccion es obligatorio", Toast.LENGTH_LONG).show();
            return false;
        }

        return  true;
    }


}
