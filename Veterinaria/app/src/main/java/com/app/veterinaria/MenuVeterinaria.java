package com.app.veterinaria;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.app.veterinaria.model.ModelUsuario;
import com.app.veterinaria.tools.DataViewList;

import org.json.JSONArray;
import org.json.JSONObject;


public class MenuVeterinaria extends ActionBarActivity {

    private String rol,personal;
    private Button btnMascota,btnPropietario,btnPersonal,btnHistorial,btnUsuario;
    private ModelUsuario modelUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        loadComponent();

        Bundle bundle = getIntent().getExtras();
        rol = bundle.getString("rol");
        personal =  bundle.getString("personalid");

        modelUsuario = new ModelUsuario();
        validarRol();
    }

    public void loadComponent(){
        btnMascota = (Button) findViewById(R.id.button4);
        btnPropietario =  (Button) findViewById(R.id.button5);
        btnPersonal = (Button) findViewById(R.id.button11);
        btnHistorial = (Button) findViewById(R.id.button12);
        btnUsuario = (Button) findViewById(R.id.button23);

        btnMascota.setVisibility(View.INVISIBLE);
        btnPropietario.setVisibility(View.INVISIBLE);
        btnPersonal.setVisibility(View.INVISIBLE);
        btnHistorial.setVisibility(View.INVISIBLE);
        btnUsuario.setVisibility(View.INVISIBLE);

    }

    public void irMascota(View view) {

        Intent intent  = new Intent(this, MenuMascota.class);
        startActivity(intent);
    }

    public void irPropietario(View view) {
        Intent intent  = new Intent(this, MenuPropietario.class);
        startActivity(intent);
    }

    public void irPersonal(View view) {
        Intent intent  = new Intent(this, MenuPersonal.class);
        startActivity(intent);
    }

    public void irHistorial(View view) {
        Intent intent  = new Intent(this, MenuHistorial.class);
        intent.putExtra("personalid",personal);
        startActivity(intent);
    }

    public void irUsuario(View view) {
        Intent intent  = new Intent(this, MenuUsuario.class);
        startActivity(intent);
    }

    public void validarRol(){
        try {
            JSONArray jsonArray = modelUsuario.paginas(rol);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject pagina = jsonArray.getJSONObject(i);
                habilitar(pagina.getInt("id"));
            }
        }catch (Exception er){}
    }

    public  void habilitar(int idPagina){
        switch (idPagina){
            case 1:
                btnMascota.setVisibility(View.VISIBLE);
                break;
            case 2:
                btnPropietario.setVisibility(View.VISIBLE);
                break;
            case 3:
                btnPersonal.setVisibility(View.VISIBLE);
                break;
            case 4:
                btnHistorial.setVisibility(View.VISIBLE);
                break;
            case 5:
                btnUsuario.setVisibility(View.VISIBLE);
                break;
        }
    }

}
