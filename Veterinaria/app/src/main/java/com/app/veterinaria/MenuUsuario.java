package com.app.veterinaria;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.app.veterinaria.R;

public class MenuUsuario extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_usuario);
    }

    public void irRegistrar(View view) {

        Intent intent  = new Intent(this, Usuario.class);
        intent.putExtra("operacion",2);
        startActivity(intent);
    }

    public void irModificar(View view) {
        Intent intent  = new Intent(this, ListaUsuario.class);
        intent.putExtra("operacion",3);
        startActivity(intent);
    }

    public void irVer(View view) {
        Intent intent  = new Intent(this, ListaUsuario.class);
        intent.putExtra("operacion", 1);
        startActivity(intent);
    }
}
