package com.app.veterinaria;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;


public class MenuPersonal extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_personal);
    }

    public void irRegistrar(View view) {
        Intent intent  = new Intent(this, Personal.class);
        intent.putExtra("operacion",2);
        startActivity(intent);
    }

    public void irModificar(View view) {
        Intent intent  = new Intent(this, ListPersonal.class);
        intent.putExtra("operacion",3);
        startActivity(intent);
    }

    public void irVer(View view) {
        Intent intent  = new Intent(this, ListPersonal.class);
        intent.putExtra("operacion",1);
        startActivity(intent);
    }

}
