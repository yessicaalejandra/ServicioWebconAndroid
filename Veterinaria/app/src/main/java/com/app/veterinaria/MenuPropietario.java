package com.app.veterinaria;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;


public class MenuPropietario extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_propietario);
    }

    public void irRegistrar(View view) {
        Intent intent  = new Intent(this, Propietario.class);
        intent.putExtra("operacion",2);
        startActivity(intent);
    }

    public void irModificar(View view) {
        Intent intent  = new Intent(this, ListPropietario.class);
        intent.putExtra("operacion",3);
        startActivity(intent);
    }

    public void irVer(View view) {
        Intent intent  = new Intent(this, ListPropietario.class);
        intent.putExtra("operacion",1);
        startActivity(intent);
    }
}
