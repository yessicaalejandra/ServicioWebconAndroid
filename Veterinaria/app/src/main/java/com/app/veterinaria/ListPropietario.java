package com.app.veterinaria;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;

import com.app.veterinaria.model.ModelPropietario;
import com.app.veterinaria.tools.AdapterListBean;
import com.app.veterinaria.tools.DataViewList;

import java.util.ArrayList;
import java.util.Locale;


public class ListPropietario extends ActionBarActivity {

    private ModelPropietario modelPropietario;
    private ListView listData;
    private EditText textBuscar;
    private AdapterListBean adapterListBean;
    private int operacion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_propietario);

        Bundle bundle = getIntent().getExtras();
        operacion = bundle.getInt("operacion");

        modelPropietario = new ModelPropietario();

        listData = (ListView) findViewById(R.id.listView);
        textBuscar = (EditText) findViewById(R.id.textBuscar);

        try {
            ArrayList<DataViewList> dataViewLists =  modelPropietario.consultar();
            if(dataViewLists != null) {
                adapterListBean = new AdapterListBean(this, dataViewLists, Propietario.class, "Nombre: ", "ID: ", true, operacion);
                listData.setAdapter(adapterListBean);
            }

            textBuscar.addTextChangedListener(new TextWatcher() {

                @Override
                public void afterTextChanged(Editable arg0) {
                    String text = textBuscar.getText().toString().toLowerCase(Locale.getDefault());
                    adapterListBean.filter(text);
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1,
                                              int arg2, int arg3) {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                    // TODO Auto-generated method stub
                }
            });
        }catch (Exception er){
            Log.e("errExceptio", er.getMessage());
        }

    }

}
