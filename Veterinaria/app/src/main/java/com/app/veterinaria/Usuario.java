package com.app.veterinaria;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.app.veterinaria.model.ModelPersonal;
import com.app.veterinaria.model.ModelRol;
import com.app.veterinaria.model.ModelUsuario;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;


public class Usuario extends ActionBarActivity {

    private String id;
    private EditText nick,pass;
    private Spinner fkpersonal,fkrol;
    private Switch estado;
    private Button btnOpe;

    private int operacion;
    private ModelUsuario modelUsuario;
    private ModelPersonal modelPersonal;
    private ModelRol modelRol;
    private String idPara;

    private HashMap<String,String> personals;
    private HashMap<String,String> rols;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario);

        modelUsuario = new ModelUsuario();
        modelPersonal = new ModelPersonal();
        modelRol = new ModelRol();

        Bundle bundle = getIntent().getExtras();
        idPara = bundle.getString("id") == null ? "-1": bundle.getString("id");
        operacion = bundle.getInt("operacion");

        loadComponent();
        loadPersonalRol();

        llamarEstado();
    }

    public  void llamarEstado(){
        switch (operacion){
            case 1:
                loadValueComponent(idPara);
                estadoVer();
                break;
            case 2:
                estadoCrear();
                break;
            case 3:
                loadValueComponent(idPara);
                estadoModificar();
                break;
        }
    }

    public void loadPersonalRol(){
        personals = modelPersonal.consultar(fkpersonal,this);
        rols = modelRol.consultar(fkrol,this);
    }

    private void estadoModificar() {
        btnOpe.setText("Modificar");
    }

    private void estadoCrear() {
        btnOpe.setText("Guardar");
    }

    private void estadoVer() {
        fkrol.setEnabled(false);
        fkpersonal.setEnabled(false);
        nick.setEnabled(false);
        pass.setEnabled(false);
        estado.setEnabled(false);
        btnOpe.setVisibility(View.INVISIBLE);
    }

    private void loadValueComponent(String idPara) {
        JSONObject jsonObject = modelUsuario.buscar(idPara);
        if(jsonObject != null){
            try {
                JSONObject jsonPersonal =  modelPersonal.buscar(jsonObject.getString("fkpersonal"));
                int posicion =  modelPersonal.getPosicion(jsonPersonal.getString("id"));
                fkpersonal.setSelection(posicion);

                JSONObject jsonRol  = modelRol.buscar(jsonObject.getString("fkrol"));
                int posicionrol =  modelRol.getPosicion(jsonRol.getString("id"));
                fkrol.setSelection(posicionrol);

                id = idPara;
                nick.setText(jsonObject.getString("nick"));
                pass.setText(jsonObject.getString("pass"));
                estado.setChecked(jsonObject.getString("estado").equals("1") ? true : false);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void loadComponent(){

        fkrol = (Spinner)  findViewById(R.id.spinner2);
        fkpersonal = (Spinner) findViewById(R.id.spinner4);
        nick = (EditText) findViewById(R.id.editText14);
        pass = (EditText) findViewById(R.id.editText15);
        estado = (Switch) findViewById(R.id.switch4);
        btnOpe = (Button) findViewById(R.id.button19);

    }

    public void clickBtn(View view){
        switch (operacion){
            case 2:
                crear();
                break;

            case 3:
                modificar();
                break;
        }
    }

    private void modificar() {

        int intEstado = estado.isChecked() ? 1 : 0;
        boolean verificacion = modelUsuario.modificar(
                id,
                nick.getText().toString(),
                pass.getText().toString(),
                rols.get(fkrol.getSelectedItem().toString()),
                String.valueOf(intEstado),
                personals.get(fkpersonal.getSelectedItem().toString())
        );

        if(verificacion){
            Toast.makeText(getApplicationContext(), "Modificado correctamente", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(getApplicationContext(), "Error al modificar", Toast.LENGTH_LONG).show();
        }

    }

    private void crear() {
        if(validarCampos()) {
            int intEstado = estado.isChecked() ? 1 : 0;

            String idNew = modelUsuario.crear(
                    "0",
                    nick.getText().toString(),
                    pass.getText().toString(),
                    rols.get(fkrol.getSelectedItem().toString()),
                    String.valueOf(intEstado),
                    personals.get(fkpersonal.getSelectedItem().toString())
            );

            if (!idNew.equals("-1")) {
                Toast.makeText(getApplicationContext(), "Guardado correctamente", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Error al guardar", Toast.LENGTH_LONG).show();
            }
        }
    }
    public  boolean validarCampos() {

        if (nick.getText().toString().trim().toLowerCase().equals("null") || nick.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "El campo Nick es obligatorio", Toast.LENGTH_LONG).show();
            return false;
        }

        if (pass.getText().toString().trim().toLowerCase().equals("null") || pass.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "El campo  Contraseņa  es obligatorio", Toast.LENGTH_LONG).show();
            return false;
        }
        return  true;
    }

}
