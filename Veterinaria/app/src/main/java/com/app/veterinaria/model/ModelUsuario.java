package com.app.veterinaria.model;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.app.veterinaria.tools.DataViewList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ModelUsuario {

    public String crear(String id,
                        String nick,
                        String pass,
                        String fkrol,
                        String estado,
                        String fkpersonal){

        String idNuevo = "-1";

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id",id );
            jsonObject.put("nick",nick);
            jsonObject.put("pass", pass);
            jsonObject.put("fkrol", fkrol);
            jsonObject.put("estado",estado);
            jsonObject.put("fkpersonal", fkpersonal);

            idNuevo =  serviceVeterinaria.PostrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/CrearUsuario", jsonObject.toString());

        }catch (Exception er){}
        return idNuevo;
    }

    public boolean modificar(String id,
                             String nick,
                             String pass,
                             String fkrol,
                             String estado,
                             String fkpersonal){

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();

        try{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id",id );
            jsonObject.put("nick",nick);
            jsonObject.put("pass", pass);
            jsonObject.put("fkrol", fkrol);
            jsonObject.put("estado",estado);
            jsonObject.put("fkpersonal", fkpersonal);

            return Boolean.parseBoolean(serviceVeterinaria.PostrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/ModificarUsuario", jsonObject.toString()));

        }catch (Exception er){}
        return true;
    }

    public JSONObject buscar(String idBuscar){

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String data = serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetUsuario/"+idBuscar);
        try {
            return new JSONObject(data);
        } catch (JSONException e) {}
        return  null;
    }

    public ArrayList<DataViewList> consultar(){
        ArrayList<DataViewList> list = new ArrayList<DataViewList>();

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String json =serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetUsuarios");

        try {
            JSONArray jsonArray = new JSONArray(json);
            for(int i=0;i<jsonArray.length();i++){
                JSONObject usuario = jsonArray.getJSONObject(i);
                list.add(new DataViewList(usuario.getString("nick"),usuario.getString("fkpersonal"),usuario.getString("id")));
            }
        }catch (Exception er){}
        return list;
    }


    public JSONObject logear(String nick,String pass){

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String json = serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/Logear/" + nick + "/" + pass + "");
        try {
            return new JSONObject(json);
        } catch (JSONException e) {}
        return  null;
    }

    public  JSONArray paginas(String rol){

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String json = serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetPaginas/"+rol);
        try {
            JSONArray listpagina =  new JSONArray(json);
            return  listpagina;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  null;
    }

}
