package com.app.veterinaria.model;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.app.veterinaria.tools.DataViewList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ModelPropietario {

    public String crear(String id,
                         String documento,
                         String tipoDocumento,
                         String nombre,
                         String direccion,
                         String telefono,
                         String correo,
                         String estado){
            String idNuevo = "-1";

            ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("id", Integer.parseInt(id));
                jsonObject.put("numdoc",documento);
                jsonObject.put("tipodoc", tipoDocumento);
                jsonObject.put("nombre", nombre);
                jsonObject.put("direccion",direccion);
                jsonObject.put("telefono", telefono);
                jsonObject.put("correo",correo);
                jsonObject.put("estado",Integer.parseInt(estado));
                jsonObject.put("mascotapropietarios", new JSONArray());

                if(!Boolean.parseBoolean(serviceVeterinaria.PostrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/PropietarioExiste", jsonObject.toString()))) {
                    idNuevo = serviceVeterinaria.PostrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/CrearPropietario", jsonObject.toString());
                }

            }catch (Exception er){}
        return idNuevo;
    }

    public boolean modificar(String id,
                             String documento,
                             String tipoDocumento,
                             String nombre,
                             String direccion,
                             String telefono,
                             String correo,
                             String estado){

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();

        try{
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("id",Integer.parseInt(id));
                jsonObject.put("numdoc",documento);
                jsonObject.put("tipodoc", tipoDocumento);
                jsonObject.put("nombre", nombre);
                jsonObject.put("direccion",direccion);
                jsonObject.put("telefono", telefono);
                jsonObject.put("correo",correo);
                jsonObject.put("estado",Integer.parseInt(estado));
                jsonObject.put("mascotapropietarios", new JSONArray());

                return Boolean.parseBoolean(serviceVeterinaria.PostrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/ModificarPropietario", jsonObject.toString()));

            }catch (Exception er){}
        return true;
    }

    public JSONObject buscar(String idBuscar){

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String response = serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetPropietario/"+idBuscar);
        try {
            JSONObject jsonObject = new JSONObject(response);
            return  jsonObject;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public ArrayList<DataViewList> consultar(){

        ArrayList<DataViewList> list = new ArrayList();
        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String json =serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetPropietarios");

        try {
            JSONArray jsonArray = new JSONArray(json);
            for(int i=0;i<jsonArray.length();i++){
                JSONObject propietario = jsonArray.getJSONObject(i);
                list.add(new DataViewList(propietario.getString("nombre"),propietario.getString("numdoc"),propietario.getString("id")));
            }
        }catch (Exception er){}
        return list;
    }

    public HashMap<String,String> consultar(Spinner spiner,Context context){

        HashMap<String,String> spinnerMap = new HashMap<String, String>();
        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String json =serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetPropietarios");

        try {
            JSONArray jsonArray = new JSONArray(json);
            String data[] = new String[jsonArray.length()];
            for(int i=0;i<jsonArray.length();i++){
                JSONObject propietario = jsonArray.getJSONObject(i);
                data[i] = propietario.getString("nombre");
                spinnerMap.put(propietario.getString("nombre"),propietario.getString("id"));
            }

            ArrayAdapter<String> adapter =new ArrayAdapter<String>(context,android.R.layout.simple_spinner_item, data);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spiner.setAdapter(adapter);


        }catch (Exception er){}
        return  spinnerMap;
    }

    public int getPosicion(String id){

        ServiceVeterinaria serviceVeterinaria =  new ServiceVeterinaria();
        String json =serviceVeterinaria.GetrequestService("http://192.168.1.50:3869/Servicio/Veterinaria.svc/GetPropietarios");
        int posicion = 0;
        try {
            JSONArray jsonArray = new JSONArray(json);
            for(int i=0;i<jsonArray.length();i++){
                JSONObject propietario = jsonArray.getJSONObject(i);
                if(propietario.getString("id").equals(id)){
                    return  i;
                }
            }

        }catch (Exception er){}
        return  posicion;
    }

    //String name = spinner.getSelectedItem().toString();
    //String id = spinnerMap.get(name);
}
