package com.app.veterinaria;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.app.veterinaria.model.ModelPropietario;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class Propietario extends ActionBarActivity {

    private String id;
    private EditText numdoc,nombre,direccion,telefono,correo;
    private Switch estado;
    private Spinner cdTipo;
    private Button btnOpe;
    private int operacion;
    private ModelPropietario modelPropietario;
    private String idPara;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_propietario);

        modelPropietario = new ModelPropietario();

        loadComponent();
        loadTipo();

        Bundle bundle = getIntent().getExtras();
        idPara = bundle.getString("id") == null ? "-1": bundle.getString("id");
        operacion = bundle.getInt("operacion");

        llamarEstado();
    }

    public  void llamarEstado(){
        switch (operacion){
            case 1:
                loadValueComponent(idPara);
                estadoVer();
                break;
            case 2:
                estadoCrear();
                break;
            case 3:
                loadValueComponent(idPara);
                estadoModificar();
                break;
        }
    }

    public void loadTipo(){

        List<String> listTipo = new ArrayList<String>();
        listTipo.add("Cedula");
        listTipo.add("Tarjeta Identidad");
        listTipo.add("Pasaporte");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listTipo);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        cdTipo.setAdapter(dataAdapter);
    }

    public void loadComponent(){

        numdoc = (EditText) findViewById(R.id.textDocumento);
        cdTipo = (Spinner) findViewById(R.id.cbTipo);
        nombre = (EditText) findViewById(R.id.textNombre);
        direccion = (EditText) findViewById(R.id.textDireccion);
        telefono = (EditText) findViewById(R.id.textTelefono);
        correo = (EditText) findViewById(R.id.textCorreo);
        estado = (Switch) findViewById(R.id.switch2);
        btnOpe = (Button) findViewById(R.id.btnGuardar);
    }

    public void estadoVer(){
        numdoc.setEnabled(false);
        cdTipo.setEnabled(false);
        nombre.setEnabled(false);
        direccion.setEnabled(false);
        telefono.setEnabled(false);
        correo.setEnabled(false);
        estado.setEnabled(false);
        btnOpe.setVisibility(View.INVISIBLE);
    }

    public  void estadoModificar(){
        btnOpe.setText("Modificar");
    }

    public  void estadoCrear(){
        btnOpe.setText("Guardar");
    }

    public  void loadValueComponent(String idPara){

        JSONObject jsonObject = modelPropietario.buscar(idPara);
        if(jsonObject != null){
            try {
                id = idPara;
                numdoc.setText(jsonObject.getString("numdoc"));
                cdTipo.setSelection(jsonObject.getInt("tipodoc"));
                nombre.setText(jsonObject.getString("nombre"));
                direccion.setText(jsonObject.getString("direccion"));
                telefono.setText(jsonObject.getString("telefono"));
                correo.setText(jsonObject.getString("correo"));
                estado.setChecked(jsonObject.getString("estado").equals("1") ? true : false);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void clickBtn(View view){
        switch (operacion){
            case 2:
                crear();
                break;

            case 3:
                modificar();
                break;
        }
    }
    public void crear(){
        if(validarCampos()) {
            int intEstado = estado.isChecked() ? 1 : 0;

            String codNew = modelPropietario.crear(
                    "0",
                    numdoc.getText().toString(),
                    String.valueOf(cdTipo.getSelectedItemPosition()),
                    nombre.getText().toString(),
                    direccion.getText().toString(),
                    telefono.getText().toString(),
                    correo.getText().toString(),
                    String.valueOf(intEstado)
            );

            if (!codNew.equals("-1")) {
                Toast.makeText(getApplicationContext(), "Guardado correctamente", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Error el propietario ya existe o Datos no validos", Toast.LENGTH_LONG).show();
            }
        }
    }

    public  void modificar(){

        if(validarCampos()){
            int intEstado = estado.isChecked() ? 1 : 0;

            boolean verificacion = modelPropietario.modificar(
                    id,
                    numdoc.getText().toString(),
                    String.valueOf(cdTipo.getSelectedItemPosition()),
                    nombre.getText().toString(),
                    direccion.getText().toString(),
                    telefono.getText().toString(),
                    correo.getText().toString(),
                    String.valueOf(intEstado)
            );

            if (verificacion) {
                Toast.makeText(getApplicationContext(), "Modificado correctamente", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Error al modificar", Toast.LENGTH_LONG).show();
            }
        }
    }

    public  boolean validarCampos(){

        if(nombre.getText().toString().trim().toLowerCase().equals("null") || nombre.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "El campo nombre es obligatorio", Toast.LENGTH_LONG).show();
            return false;
        }

        if(numdoc.getText().toString().trim().toLowerCase().equals("null") || numdoc.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "El campo documento es obligatorio", Toast.LENGTH_LONG).show();
            return false;
        }

        if(direccion.getText().toString().trim().toLowerCase().equals("null") || direccion.getText().toString().trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "El campo direccion es obligatorio", Toast.LENGTH_LONG).show();
            return false;
        }

        return  true;
    }


}
