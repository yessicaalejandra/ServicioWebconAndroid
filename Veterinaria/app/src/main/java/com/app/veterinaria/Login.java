package com.app.veterinaria;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.app.veterinaria.model.ModelUsuario;

import org.json.JSONObject;


public class Login extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void logear(View view) {

        ModelUsuario modelUsuario = new ModelUsuario();

        EditText nick = (EditText) findViewById(R.id.textNick);
        EditText pass = (EditText) findViewById(R.id.textPass);

        JSONObject jsonObject =  modelUsuario.logear(nick.getText().toString(), pass.getText().toString());
        try {
            Intent intent  = new Intent(this, MenuVeterinaria.class);
            intent.putExtra("rol", jsonObject.getString("fkrol"));
            intent.putExtra("personalid", jsonObject.getString("fkpersonal"));
            Toast.makeText(getApplicationContext(), "Bienvenido", Toast.LENGTH_LONG).show();
            startActivity(intent);
        }catch (Exception er){
            Toast.makeText(getApplicationContext(), "Error al logear", Toast.LENGTH_LONG).show();
        }
    }

}
