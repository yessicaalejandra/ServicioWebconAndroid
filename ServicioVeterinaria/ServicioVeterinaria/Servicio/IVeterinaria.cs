﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ServicioVeterinaria.Model;
using System.ServiceModel.Web;

namespace ServicioVeterinaria.Servicio
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IVeterinaria" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IVeterinaria
    {

        [WebInvoke(UriTemplate = "GetMascotas", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<mascota> getMascotas();

        [WebGet(UriTemplate = "GetMascota/{id}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        mascota getMascota(string id);

        [OperationContract, WebInvoke(UriTemplate = "CrearMascota", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string AddMascota(mascota newMascota);

        [OperationContract, WebInvoke(UriTemplate = "ModificarMascota", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean ModifyMascota(mascota newMascota);


        [WebInvoke(UriTemplate = "GetPersonals", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<personal> getPersonals();

        [WebGet(UriTemplate = "GetPersonal/{id}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        personal getPersonal(string id);

        [OperationContract, WebInvoke(UriTemplate = "CrearPersonal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string AddPersonal(personal newPersonal);

        [OperationContract, WebInvoke(UriTemplate = "ModificarPersonal", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean ModifyPersonal(personal newPersonal);


        [WebInvoke(UriTemplate = "GetPropietarios", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<propietario> getPropietarios();

        [WebGet(UriTemplate = "GetPropietario/{id}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        propietario getPropietario(string id);

        [OperationContract, WebInvoke(UriTemplate = "CrearPropietario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string AddPropietario(propietario newPropietario);

        [OperationContract, WebInvoke(UriTemplate = "ModificarPropietario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean ModifyPropietario(propietario newPropietario);


        [WebInvoke(UriTemplate = "GetHistorials", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<historial> getHistorials();

        [WebGet(UriTemplate = "GetHistorial/{id}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        historial getHistorial(string id);

        [OperationContract, WebInvoke(UriTemplate = "CrearHistorial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string AddHistorial(historial newHistorial);

        [OperationContract, WebInvoke(UriTemplate = "ModificarHistorial", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean ModifyHistorial(historial newHistorial);


        [WebInvoke(UriTemplate = "GetMascotaPropietarios", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<mascotapropietario> getMascotaPropietarios();

        [WebGet(UriTemplate = "GetMascotaPropietario/{id}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        mascotapropietario getMascotaPropietario(string id);

        [OperationContract, WebInvoke(UriTemplate = "CrearMascotaPropietario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string AddMascotaPropietario(mascotapropietario newMascotaPropietario);

        [OperationContract, WebInvoke(UriTemplate = "ModificarMascotaPropietario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean ModifyMascotaPropietario(mascotapropietario newMascotaPropietario);


        [WebInvoke(UriTemplate = "GetUsuarios", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<usuario> getUsuarios();

        [WebGet(UriTemplate = "GetUsuario/{id}", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        usuario getUsuario(string id);

        [OperationContract, WebInvoke(UriTemplate = "CrearUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string AddUsuario(usuario newUsuario);

        [OperationContract, WebInvoke(UriTemplate = "ModificarUsuario", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean ModifyUsuario(usuario newUsuario);

        [WebInvoke(UriTemplate = "Logear/{nick}/{pass}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        usuario logear(string nick,string pass);


        [WebInvoke(UriTemplate = "GetPaginas/{rol}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<pagina> getPaginas(string rol);

        [WebInvoke(UriTemplate = "GetRols", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        List<rol> getRols();

        [WebInvoke(UriTemplate = "GetRol/{id}", Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        rol getRol(string id);


        [OperationContract, WebInvoke(UriTemplate = "PropietarioExiste", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean PropietarioExiste(propietario newPropietario);


        [OperationContract, WebInvoke(UriTemplate = "PersonalExiste", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        Boolean PersonalExiste(personal newPersonal);
    }
}
