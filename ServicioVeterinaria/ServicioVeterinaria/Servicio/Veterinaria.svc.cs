﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ServicioVeterinaria.Model;
using System.Data.Entity;

namespace ServicioVeterinaria.Servicio
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Veterinaria" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Veterinaria.svc o Veterinaria.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Veterinaria : IVeterinaria
    {
        
        veterinariaEntities model = new veterinariaEntities();

        #region Mascota
        public List<mascota> getMascotas()
        {
            try
            {
                List<mascota> Listmascota = new List<mascota>();                
                Listmascota = model.mascotas.ToList();
                return Listmascota;
            }
            catch (Exception) {
                return new List<mascota>();
            }            
        }


        public mascota getMascota(string id)
        {
            try
            {
                int idMascota = Int32.Parse(id);                
                return model.mascotas.SingleOrDefault(p => p.id == idMascota);
            }
            catch (Exception) {
                return new mascota();
            }            
        }


        public string AddMascota(mascota newMascota)
        {
            try
            {                
                model.mascotas.Add(newMascota);
                model.SaveChanges();
                return newMascota.id.ToString();
            }
            catch (Exception) {
                return "-1";
            }            
        }   


        public Boolean ModifyMascota(mascota newMascota)
        {
            try
            {
                mascota oldMascota = model.mascotas.SingleOrDefault(p => p.id == newMascota.id);

                if (oldMascota == null)
                {
                    return false;
                }

                oldMascota.color = newMascota.color;
                oldMascota.edad = newMascota.edad;
                oldMascota.especie = newMascota.especie;
                oldMascota.estado = newMascota.estado;
                oldMascota.nombre = newMascota.nombre;
                oldMascota.raza = newMascota.raza;
                oldMascota.sexo = newMascota.sexo;                

                if (model.ChangeTracker.HasChanges())
                {
                    model.SaveChanges();
                }
                
                return true;
            }
            catch (Exception) {
                return false;
            }            
        }
        #endregion

        #region Personal
        public List<personal> getPersonals()
        {
            try
            {
                List<personal> Listpersonal = new List<personal>();                
                Listpersonal = model.personals.ToList();
                return Listpersonal;
            }
            catch (Exception) {
                return new List<personal>();
            }            
        }


        public personal getPersonal(string id)
        {
            try
            {
                int idPersonal = Int32.Parse(id);                
                return model.personals.SingleOrDefault(p => p.id == idPersonal);
            }
            catch (Exception) {
                return new personal();
            }            
        }


        public string AddPersonal(personal newPersonal)
        {
            try
            {                
                model.personals.Add(newPersonal);
                model.SaveChanges();
                return newPersonal.id.ToString();
            }
            catch (Exception) {
                return "-1";
            }            
        }


        public Boolean ModifyPersonal(personal newPersonal)
        {
            try
            {
                personal  oldPersonal =  model.personals.SingleOrDefault(p => p.id == newPersonal.id);

                if(oldPersonal == null){
                    return false;
                }

                oldPersonal.correo = newPersonal.correo;
                oldPersonal.direccion = newPersonal.direccion;
                oldPersonal.estado = newPersonal.estado;
                oldPersonal.nombre = newPersonal.nombre;
                oldPersonal.numdoc = newPersonal.numdoc;
                oldPersonal.telefono = newPersonal.telefono;
                oldPersonal.tipodoc = newPersonal.tipodoc;

                if (model.ChangeTracker.HasChanges())
                {
                    model.SaveChanges();
                }

                return true;
            }
            catch (Exception) {
                return false;
            }            
        }
        #endregion

        #region Propietario
        public List<propietario> getPropietarios()
        {
            try
            {
                List<propietario> Listpropietario = new List<propietario>();                
                Listpropietario = model.propietarios.ToList();
                return Listpropietario;
            }
            catch (Exception) {
                return new List<propietario>();
            }            
        }


        public propietario getPropietario(string id)
        {
            try
            {
                int idPropietario = Int32.Parse(id);                
                return model.propietarios.SingleOrDefault(p => p.id == idPropietario);
            }
            catch (Exception) {
                return new propietario();
            }            
        }


        public string AddPropietario(propietario newPropietario)
        {
            try
            {                
                model.propietarios.Add(newPropietario);
                model.SaveChanges();
                return newPropietario.id.ToString();
            }
            catch (Exception) {
                return "-1";
            }            
        }


        public Boolean ModifyPropietario(propietario newPropietario)
        {
            try
            {
                propietario oldPropietario = model.propietarios.SingleOrDefault(p => p.id == newPropietario.id);

                if(oldPropietario == null){
                    return false;
                }

                oldPropietario.correo = newPropietario.correo;
                oldPropietario.direccion = newPropietario.direccion;
                oldPropietario.estado = newPropietario.estado;
                oldPropietario.nombre = newPropietario.nombre;
                oldPropietario.numdoc = newPropietario.numdoc;
                oldPropietario.telefono = newPropietario.telefono;
                oldPropietario.tipodoc = newPropietario.tipodoc;

                if (model.ChangeTracker.HasChanges())
                {
                    model.SaveChanges();
                }

                return true;
            }
            catch (Exception) {
                return false;
            }            
        }
        #endregion

        #region historial 
        public List<historial> getHistorials()
        {
            try
            {
                List<historial> Listhistorial = new List<historial>();                
                Listhistorial = model.historials.ToList();
                return Listhistorial;
            }
            catch (Exception) {
                return new List<historial>();
            }            
        }


        public historial getHistorial(string id)
        {
            try
            {
                int idHistorial = Int32.Parse(id);                
                return model.historials.SingleOrDefault(p => p.id == idHistorial);
            }
            catch (Exception) {
                return new historial();
            }
            
        }


        public string AddHistorial(historial newHistorial)
        {
            try
            {                
                model.historials.Add(newHistorial);
                model.SaveChanges();
                return newHistorial.id.ToString();
            }
            catch (Exception) {
                return "-1";
            }            
        }


        public Boolean ModifyHistorial(historial newHistorial)
        {
            try
            {
                historial oldHistorial = model.historials.SingleOrDefault(p => p.id == newHistorial.id);

                if(oldHistorial == null){
                    return false;
                }

                oldHistorial.anamesia = newHistorial.anamesia;
                oldHistorial.diagnostico = newHistorial.diagnostico;
                oldHistorial.fecha = newHistorial.fecha;
                oldHistorial.fechavacuna = newHistorial.fechavacuna;
                oldHistorial.hora = newHistorial.hora;
                oldHistorial.peso = newHistorial.peso;
                oldHistorial.tratamiento = newHistorial.tratamiento;
                oldHistorial.vacuna = newHistorial.vacuna;

                if (model.ChangeTracker.HasChanges())
                {
                    model.SaveChanges();                    
                }
                return true;
            }
            catch (Exception) {
                return false;
            }            
        }
        #endregion

        #region Mascota Propietario
        public List<mascotapropietario> getMascotaPropietarios()
        {
            try
            {
                List<mascotapropietario> ListmascotaPropietario = new List<mascotapropietario>();                
                ListmascotaPropietario = model.mascotapropietarios.ToList();
                return ListmascotaPropietario;
            }
            catch (Exception) {
                return new List<mascotapropietario>();
            }            
        }


        public mascotapropietario getMascotaPropietario(string id)
        {
            try
            {
                int fkMascota = Int32.Parse(id);
                mascotapropietario oldmp =  model.mascotapropietarios.Where(p => p.fkmascota == fkMascota).OrderByDescending(f => f.fecha).FirstOrDefault();

                if(oldmp == null){
                    return new mascotapropietario();
                }

                return oldmp;
            }
            catch (Exception) {
                return new mascotapropietario();
            }            
        }


        public string AddMascotaPropietario(mascotapropietario newMascotaPropietario)
        {
            try
            {                
                model.mascotapropietarios.Add(newMascotaPropietario);
                model.SaveChanges();
                return newMascotaPropietario.fkmascota.ToString();
            }
            catch (Exception) {
                return "-1";
            }            
        }

        public Boolean ModifyMascotaPropietario(mascotapropietario newMascotaPropietario)
        {
            try
            {
                mascotapropietario oldmp = model.mascotapropietarios.SingleOrDefault(p => p.fkmascota == newMascotaPropietario.fkmascota && p.fkpropietario== newMascotaPropietario.fkpropietario );

                if(oldmp == null){
                    return false;
                }

                oldmp.fecha = newMascotaPropietario.fecha;

                if (model.ChangeTracker.HasChanges())
                {
                    model.SaveChanges();
                }
                return true;
            }
            catch (Exception) {
                return false;
            }            
        }
        #endregion

        #region Usuario
        public List<usuario> getUsuarios()
        {
            try
            {
                List<usuario> Listusuario = new List<usuario>();                
                Listusuario = model.usuarios.ToList();
                return Listusuario;
            }
            catch (Exception) {
                return new List<usuario>();
            }            
        }


        public usuario getUsuario(string id)
        {
            try
            {
                int idUsuario = Int32.Parse(id);                
                return model.usuarios.SingleOrDefault(p => p.id == idUsuario);
            }
            catch (Exception) {
                return new usuario();
            }            
        }


        public string AddUsuario(usuario newUsuario)
        {
            try
            {                
                model.usuarios.Add(newUsuario);
                model.SaveChanges();
                return newUsuario.id.ToString();
            }
            catch (Exception) {
                return "";
            }            
        }


        public Boolean ModifyUsuario(usuario newUsuario)
        {
            try
            {
                usuario oldUsuario = model.usuarios.SingleOrDefault(p => p.id == newUsuario.id);
               
                if(oldUsuario == null){
                    return false;
                }

                oldUsuario.estado = newUsuario.estado;
                oldUsuario.nick = newUsuario.nick;
                oldUsuario.pass = newUsuario.pass;
                oldUsuario.fkrol = newUsuario.fkrol;

                if (model.ChangeTracker.HasChanges())
                {
                    model.SaveChanges();
                }
                return true;
            }
            catch (Exception) {
                return false;
            }            
        }

       public usuario logear(string nick, string pass) {

            try {
                usuario usuarioLogeado =  model.usuarios.SingleOrDefault(p => p.nick == nick && p.pass==pass);

                if(usuarioLogeado == null){
                    return null;
                }
                return usuarioLogeado;
            }catch(Exception er){}
            return null;
       }
           
        #endregion

       public List<pagina> getPaginas(string rol) {
            int id = Convert.ToInt32(rol);           
            return model.rolpaginas.Where(x => x.fkrol == id).Select(x => x.pagina).ToList();            
       }

       public List<rol> getRols() {

           try
           {
               List<rol> Listrol = new List<rol>();
               Listrol = model.rols.ToList();
               return Listrol;
           }
           catch (Exception)
           {
               return new List<rol>();
           }

       }

       public rol getRol(string id) {

           try
           {
               int idRol = Int32.Parse(id);
               return model.rols.SingleOrDefault(p => p.id == idRol);
           }
           catch (Exception)
           {
               return new rol();
           }           
       }

       public Boolean PropietarioExiste(propietario newPropietario) {
           try {
               return model.propietarios.Count(p => p.numdoc.Trim() == newPropietario.numdoc.Trim()) > 0;
           }catch(Exception){}
           return false;
       }

       public Boolean PersonalExiste(personal newPersonal) {
           try {
               return model.personals.Count(p => p.numdoc.Trim() == newPersonal.numdoc.Trim()) > 0;
           }catch(Exception){}
           return false;
       }
    
    }
}
